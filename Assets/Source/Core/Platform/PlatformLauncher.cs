﻿namespace Developer.Core
{
    using UnityEngine;
    using UnityEngine.SceneManagement;

    /// <inheritdoc />
    /// <summary>
    /// Controller which loads all required plugins before loading the first screen for the app.
    /// </summary>
    public sealed class PlatformLauncher : MonoBehaviour
    {
        private const bool UseReporter = true;

        private App _app = null;
        string reportScene = "ReporterScene";

        private void Awake()
        {
            if (UseReporter)
            {
                //TODO: Load the reporter...


                //pretty sure this is wrong, it only works when i add the reportScene in the buiding setting
                //SceneManager.LoadSceneAsync(reportScene);

            }

            _app = new App(GameObject.Find("Canvas"));
            _app.ChangeScreen<Developer.Screen.LoadingScreen>();
        }
    }
}