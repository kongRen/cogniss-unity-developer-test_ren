﻿namespace Developer.Screen
{
    using UnityEngine;
    using System.Threading.Tasks;

    /// <inheritdoc />
    /// <summary>
    /// Controller for the screen that displays a <see cref="Developer.Component.TrafficLight" />.
    /// </summary>
    public sealed class LightScreen : Core.ScreenAbstract
    {
        private Developer.Component.TrafficLight lights = null;

        private void Awake()
        {
            GameObject trafficLightPrefab = Resources.Load<GameObject>("Prefabs/Component/TrafficLight/TrafficLight");
            GameObject trafficLightInstance = Instantiate(trafficLightPrefab, transform.Find("TrafficLightHolder"));
            lights = trafficLightInstance.AddComponent<Developer.Component.TrafficLight>();
            lights.SetState();
        }

        //TODO: According the rules, turn the lights on and off...
        //Hint: The networking functionality is inside the "Test" namespace.

        private async void Update()
        {
            await Task.Delay(3000);//not working properly
            //geting the network detail
            int network = await Test.NetworkService.GetCurrentLightValue();

            //check the condition and give feeback to decide the color
            if (network % 3 == 0 && network % 5 == 0)
            {
                lights.SetState(Test.LightColor.Green);
            }
            else if (network % 5 == 0)
            {
                lights.SetState(Test.LightColor.Yellow);
            }
            else if (network % 3 == 0)
            {
                lights.SetState(Test.LightColor.Red);
            }
            else
            {
                lights.SetState();
            }

        }

    }
}